package GiphyMatHooker;
use Mojo::Base 'Mojolicious';
use Mojo::Upload;

sub startup {
    my $self = shift;

    $self->plugin('Config', {
        default => {
            apikey => 'dc6zaTOxFJmzC',
        }
    });

    $self->plugin('DebugDumperHelper');

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->any('/' => sub {
        my $c = shift;
        my $text = $c->param('text');
        if ($c->req->method eq 'GET') {
            my @params = split('&', $c->req->content->asset->slurp);
            for my $param (@params) {
                my @h = split('=', $param);
                $c->param($h[0] => $h[1]);
                $text = $h[1] if ($h[0] eq 'text');
            }
        }
        my ($token, $channel_id, $channel_name, $command, $team_domain, $team_id, $user_id, $user_name, $response_url) =
           ($c->param('token'), $c->param('channel_id'), $c->param('channel_name'), $c->param('command'), $c->param('team_domain'), $c->param('team_id'), $c->param('user_id'), $c->param('user_name'), $c->param('response_url'));
        # This is only to check that the request comes (apparently) from mattermost
        # I know that I should test the token, but it would prevent other users from
        # framateam to use this service
        # To enable token authent, just set a token in the configuration file
        if ((!defined($c->config('token')) &&
             defined($token) &&
             defined($channel_id) &&
             defined($channel_name) &&
             defined($command) &&
             defined($team_domain) &&
             defined($team_id) &&
             defined($user_id) &&
             defined($user_name) &&
             defined($response_url))
             || (defined $c->config('token') && $token eq $c->config('token'))
        ) {
            my $url = Mojo::URL->new('https://api.giphy.com/v1/gifs/search');
            $url->query(
                q       => $text,
                api_key => $c->config('apikey'),
                limit   => 25
            );
            $c->ua->get($url => sub {
                my ($ua, $tx) = @_;
                if (my $res = $tx->success) {
                    if ($res->json->{pagination}->{total_count}) {
                        my $gif = $res->json->{data}->[rand(scalar(@{$res->json->{data}}))]->{images}->{original}->{url};
                        $gif    = $text.' ⟶ '.$gif unless (defined($c->param('notext')) && $c->param('notext'));
                        $c->render(
                            json => {
                                "response_type" => "in_channel",
                                "text"          => $gif
                            }
                        );
                    } else {
                        $c->render(
                            json => {
                                "response_type" => "ephemeral",
                                "text"          => "Sorry, no results from giphy"
                            }
                        );
                    }
                } else {
                    my $err = $tx->error;
                    my $text = ($err->{code}) ? "Sorry, $err->{code} response: $err->{message}" : "Sorry, $err->{message}";
                    $c->render(
                        json => {
                            "response_type" => "ephemeral",
                            "text"          => $text
                        }
                    );
                }
            });

            $c->render_later;
        } else {
            $c->rendered(401);
        }
    });
    $r->any('/understood' => sub {
        shift->render(
            json => {
                "response_type" => "in_channel",
                "text"          => "https://media4.giphy.com/media/cRpEcJPIBEHPG/giphy.gif"
            }
        );
    });
    $r->any('/money' => sub {
        shift->render(
            json => {
                "response_type" => "in_channel",
                "text"          => "https://media0.giphy.com/media/2mXJvHKUYL9n2/giphy.gif"
            }
        );
    });
    $r->any('/steven' => sub {
        my $c = shift;
        my $text = $c->param('text');
        if ($c->req->method eq 'GET') {
            my @params = split('&', $c->req->content->asset->slurp);
            for my $param (@params) {
                my @h = split('=', $param);
                $c->param($h[0] => $h[1]);
                $text = $h[1] if ($h[0] eq 'text');
            }
        }
        my ($token, $channel_id, $channel_name, $command, $team_domain, $team_id, $user_id, $user_name, $response_url) =
           ($c->param('token'), $c->param('channel_id'), $c->param('channel_name'), $c->param('command'), $c->param('team_domain'), $c->param('team_id'), $c->param('user_id'), $c->param('user_name'), $c->param('response_url'));
        if ((!defined($c->config('token')) &&
             defined($token) &&
             defined($channel_id) &&
             defined($channel_name) &&
             defined($command) &&
             defined($team_domain) &&
             defined($team_id) &&
             defined($user_id) &&
             defined($user_name) &&
             defined($response_url))
             || (defined $c->config('token') && $token eq $c->config('token'))
        ) {
            my $q = 'Steven Universe';
            $q   .= ' '.$text if defined $text;
            my $url = Mojo::URL->new('https://api.giphy.com/v1/gifs/search');
            $url->query(
                q       => $q,
                api_key => $c->config('apikey'),
                limit   => 200
            );
            $c->ua->get($url => sub {
                my ($ua, $tx) = @_;
                if (my $res = $tx->success) {
                    if ($res->json->{pagination}->{total_count}) {
                        my $gif = $res->json->{data}->[rand(scalar(@{$res->json->{data}}))]->{images}->{original}->{url};
                        $gif    = $c->param('text').' ⟶ '.$gif unless (defined($c->param('notext')) && $c->param('notext'));
                        $c->render(
                            json => {
                                "response_type" => "in_channel",
                                "text"          => $gif
                            }
                        );
                    } else {
                        $c->render(
                            json => {
                                "response_type" => "ephemeral",
                                "text"          => "Sorry, no results from giphy"
                            }
                        );
                    }
                } else {
                    my $err = $tx->error;
                    my $text = ($err->{code}) ? "Sorry, $err->{code} response: $err->{message}" : "Sorry, $err->{message}";
                    $c->render(
                        json => {
                            "response_type" => "ephemeral",
                            "text"          => $text
                        }
                    );
                }
            });

            $c->render_later;
        } else {
            $c->rendered(401);
        }
    });
    $r->any('/cat' => sub {
        my $c = shift;
        my $text = $c->param('text');
        if ($c->req->method eq 'GET') {
            my @params = split('&', $c->req->content->asset->slurp);
            for my $param (@params) {
                my @h = split('=', $param);
                $c->param($h[0] => $h[1]);
                $text = $h[1] if ($h[0] eq 'text');
            }
        }
        my ($token, $channel_id, $channel_name, $command, $team_domain, $team_id, $user_id, $user_name, $response_url) =
           ($c->param('token'), $c->param('channel_id'), $c->param('channel_name'), $c->param('command'), $c->param('team_domain'), $c->param('team_id'), $c->param('user_id'), $c->param('user_name'), $c->param('response_url'));
        if ((!defined($c->config('token')) &&
             defined($token) &&
             defined($channel_id) &&
             defined($channel_name) &&
             defined($command) &&
             defined($team_domain) &&
             defined($team_id) &&
             defined($user_id) &&
             defined($user_name) &&
             defined($response_url))
             || (defined $c->config('token') && $token eq $c->config('token'))
        ) {
            my $url = Mojo::URL->new('http://cataas.com/cat/gif');
            my $alt = 'Gif de chat';
            if ($text) {
                $alt = $text;
                $alt =~ s/([\[\]\(\)])/\\$1/g;
                $text =~ s/'/%27/g;
                $url->path($url->path."/says/".$text);
            }
            $c->render(
                json => {
                    "response_type" => "in_channel",
                    "text"          => "![$alt]($url)"
                }
            );
        } else {
            $c->rendered(401);
        }
    });

    $r->any('/omg' => sub {
        my $c = shift;
        my $text = $c->param('text');
        if ($c->req->method eq 'GET') {
            my @params = split('&', $c->req->content->asset->slurp);
            for my $param (@params) {
                my @h = split('=', $param);
                $c->param($h[0] => $h[1]);
                $text = $h[1] if ($h[0] eq 'text');
            }
        }
        my ($token, $channel_id, $channel_name, $command, $team_domain, $team_id, $user_id, $user_name, $response_url) =
           ($c->param('token'), $c->param('channel_id'), $c->param('channel_name'), $c->param('command'), $c->param('team_domain'), $c->param('team_id'), $c->param('user_id'), $c->param('user_name'), $c->param('response_url'));
        if ((!defined($c->config('token')) &&
             defined($token) &&
             defined($channel_id) &&
             defined($channel_name) &&
             defined($command) &&
             defined($team_domain) &&
             defined($team_id) &&
             defined($user_id) &&
             defined($user_name) &&
             defined($response_url))
             || (defined $c->config('token') && $token eq $c->config('token'))
        ) {
            my $url = Mojo::URL->new('https://omg.phie.ovh/get.php');
            $url->query(
                query => $text,
            );
            $c->ua->get($url => sub {
                my ($ua, $tx) = @_;
                if (my $res = $tx->success) {
                    if (scalar(@{$res->json})) {
                        my $gif = $res->json->[rand(scalar(@{$res->json}))]->{url};
                        if ($gif !~ m/^http/) {
                            $gif = 'https://omg.phie.ovh/'.$gif;
                        }
                        $gif    = $text.' ⟶ '.$gif unless (defined($c->param('notext')) && $c->param('notext'));
                        $c->render(
                            json => {
                                "response_type" => "in_channel",
                                "text"          => $gif
                            }
                        );
                    } else {
                        $c->render(
                            json => {
                                "response_type" => "ephemeral",
                                "text"          => "Sorry, no results from OhMyGif"
                            }
                        );
                    }
                } else {
                    my $err = $tx->error;
                    my $text = ($err->{code}) ? "Sorry, $err->{code} response: $err->{message}" : "Sorry, $err->{message}";
                    $c->render(
                        json => {
                            "response_type" => "ephemeral",
                            "text"          => $text
                        }
                    );
                }
            });

            $c->render_later;
        } else {
            $c->rendered(401);
        }
    });
}

1;
