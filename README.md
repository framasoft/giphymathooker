[![](https://framagit.org/assets/gitlab_logo-cdf021b35c4e6bb149e26460f26fae81e80552bc879179dd80e9e9266b14e894.png)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Giphy Mat Hooker

Powered By Giphy

The `Mat` stand for [Mattermost](http://www.mattermost.org/).

This service allows you to create [slash commands](http://docs.mattermost.com/developer/slash-commands.html) on Mattermost to search gifs on <http://giphy.com/>.

## License

Giphy Mat Hooker is licensed under the terms of the AGPLv3. See the LICENSE file.

# Installation

## Dependencies

* Some packages

```shell
apt-get install build-essential libssl-dev
```

* Carton : Perl dependencies manager, it will get what you need, so don't bother for dependencies (but you can read the file `cpanfile` if you want).

```shell
sudo cpan Carton
```

## The real installation
After installing Carton :

```shell
git clone https://git.framasoft.org/framasoft/giphymathooker.git
cd giphymathooker
carton install
cp giphy_mat_hooker.conf.template giphy_mat_hooker.conf
vi giphy_mat_hooker.conf
```

The configuration file is self-documented.

## Usage

### Launch manually

This is good for test, not for production.

```
carton exec hypnotoad script/giphy_mat_hooker
# stop it
carton exec hypnotoad -s script/giphy_mat_hooker
```

Yup, that's all, it will listen at "http://127.0.0.1:8080".

For more options (interfaces, user, etc.), change the configuration in `giphy_mat_hooker.conf` (have a look at http://mojolicio.us/perldoc/Mojo/Server/Hypnotoad#SETTINGS for the available options).

### Systemd

```
sudo su
cp utilities/giphymathooker.service /etc/systemd/system/
vi /etc/systemd/system/giphymathooker.service
systemctl daemon-reload
systemctl enable giphymathooker.service
systemctl start giphymathooker.service
```

## Nginx

You may want to put GHM behind a reverse proxy. Have a look at `utilities/giphymathooker.nginx` for an example.

# How to use in Mattermost?

First, have a look at the [Mattermost documentation about slash commands](http://docs.mattermost.com/developer/slash-commands.html).

Then, create your slash command, using the URL of your installation (say https://giphy.example.org), choose `GET` or `POST` method, it doesn't matter, GMH will answer to both.

Finally, put the token provided by Mattermost in your GMH configuration file. Or not, if you want anybody to be able to use it if he provides the mattermost parameters.

Now, you can search gifs on Giphy with `/gif my search terms` (if you choose `gif` as your slash command).

## Parameter

By default, GMH responds `research text ⟶ https://the_gif_url`. If you set your slash command to https:///giphy.example.org?notext=1, you will only get `https://the_gif_url`.

## Bonus

Point a slash command to https://giphy.example.org/understood/, you'll get this gif:

![Jack Sparrow saying he understand everything](https://media4.giphy.com/media/cRpEcJPIBEHPG/giphy.gif)

Point a slash command to https://giphy.example.org/money/, you'll get this gif:

![Man laughing under a banknotes rain](https://media0.giphy.com/media/2mXJvHKUYL9n2/giphy.gif)

Point a slash command to https://giphy.example.org/steven and the research will be prepended with "Steven Universe".

Point a slash command to https://giphy.example.org/cat/, you'll get a gif of a cat from [Cat as a service](http://cataas.com/). If you send text with that command, the text will be added the gif.

Point a slash command to https://giphy.example.org/omg/, you'll use the [OhMyGif](https://github.com/PhieF/OhMyGif) instance <https://omg.phie.ovh/> instead of Giphy (OhMyGif is open source, that's kind of cool).
