[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Giphy Mat Hooker

Propulsé par Giphy

Le mot `Mat` est un diminutif de [Mattermost](http://www.mattermost.org/).

Ce service vous permet de créer des "[slash commands](http://docs.mattermost.com/developer/slash-commands.html)" (ou commandes slash, commencant par le caractère `/`) dans un chat Mattermost pour chercher et afficher des gifs depuis <http://giphy.com/>.

## Licence

Giphy Mat hooker est proposé sous licence AGPLv3. Veuillez vous référer au fichier LICENSE.

# Installation

## Dépendances

* Quelques paquets de votre système

```shell
apt-get install build-essential libssl-dev
```

* Carton : Gestionnaire de dépendances Perl qui vous permettra de récupérer les dépendances nécessaires sans vous obliger à les traiter individuellement (mais rien ne vous empêche de consulter le fichier `cpanfile`).

```shell
sudo cpan Carton
```

## La véritable installation
Après avoir installé Carton :

```shell
git clone https://git.framasoft.org/framasoft/giphymathooker.git
cd giphymathooker
carton install
cp giphy_mat_hooker.conf.template giphy_mat_hooker.conf
vi giphy_mat_hooker.conf
```

Le fichier de configuration contient les explications nécessaires.

## Usage

### Lancement manuel

Cette méthode est utile pour les tests, ne pas l'utiliser en production.

```
carton exec hypnotoad script/giphy_mat_hooker
# stop it
carton exec hypnotoad -s script/giphy_mat_hooker
```
Oui, c'est tout. Le service écoutera à l'adresse "http://127.0.0.1:8080".

Pour plus d'options (interfaces, utilisateurs, etc.), vous pouvez modifier la configuration dans le fichier `giphy_mat_hooker.conf` (jetez un oeil à l'adresse http://mojolicio.us/perldoc/Mojo/Server/Hypnotoad#SETTINGS pour la liste complète des options).

### Systemd

```
sudo su
cp utilities/giphymathooker.service /etc/systemd/system/
vi /etc/systemd/system/giphymathooker.service
systemctl daemon-reload
systemctl enable giphymathooker.service
systemctl start giphymathooker.service
```

## Nginx

Si vous souhaitez installer GHM derrière un reverse proxy. Consultez le fichier d'exemple `utilities/giphymathooker.nginx`.

# Comment l'utiliser dans Mattermost?

Jetez tout d'abord un oeil à la [documentation Mattermost sur les slash commands](http://docs.mattermost.com/developer/slash-commands.html).

Créez ensuite votre commande slash en renseignant l'URL de  l'installation (par exemple https://giphy.example.org), puis choisissez entre les méthodes `GET` ou `POST`; cela n'a pas d'importance, GMH est capable de répondre aux deux méthodes.

Enfin, renseignez le jeton (*token*) fourni par Mattermost dans le fichier de configuration de GMH. Si vous ne le renseignez pas, n'importe quel service sera capable de le solliciter s'il fournit les paramètres de Mattermost.

Vous pouvez désormais chercher des gifs sur Giphy avec la commande `/gif ma recherche` (si vous avez choisi `gif` comme mot clef pour la commande slash).

## Paramètre

Par défaut, GMH répondra `texte recherché ⟶ https://url_du_gif`. Si vous utilisez https:///giphy.example.org?notext=1 dans votre slash command, vous n'aurez que `https://url_du_gif`.

## Bonus

Si vous créez une commande slash pour l'adresse https://giphy.example.org/understood/, vous obtiendrez ce gif:

![Jack Sparrow disant qu'il comprend tout](https://media4.giphy.com/media/cRpEcJPIBEHPG/giphy.gif)

Une commande slash vers https://giphy.example.org/money/ vous donnera ce gif:

![Edgar de la cambriole riant sous une pluie de billets](https://media0.giphy.com/media/2mXJvHKUYL9n2/giphy.gif)

Une commande slash vers https://giphy.example.org/steven ajoutera "Steven Universe" au début du texte recherché.

Une commande slash vers https://giphy.example.org/cat/, vous donnera un gif de [Cat as a service](http://cataas.com/). Si vous ajoutez du texte à cette commande, le texte sera ajouté au gif.

Une commande slash vers https://giphy.example.org/omg/, vous utiliserez l’instance [OhMyGif](https://github.com/PhieF/OhMyGif) <https://omg.phie.ovh/> à la place de Giphy (OhMyGif est open source, c’est plutôt cool).
